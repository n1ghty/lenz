package main

import (
	"fmt"
	"log"
	"os/exec"
	"runtime"
)

func getAptPath() string {
	path, err := exec.LookPath("apt")
	if err != nil {
		log.Fatal("apt not found")
	}
	return path
}
func update() {

	pathArgs := "update"
	cmd := exec.Command(getAptPath(), pathArgs)
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + " " + string(output))
	} else {
		fmt.Println(string(output))
	}

}

func upgrade() {

	pathArgs := []string{"upgrade", "-y"}
	cmd := exec.Command(getAptPath(), pathArgs...)
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + " " + string(output))
	} else {
		fmt.Println(string(output))
	}
}

func autoremove() {

	pathArgs := []string{"autoremove", "-y"}
	cmd := exec.Command(getAptPath(), pathArgs...)
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + " " + string(output))
	} else {
		fmt.Println(string(output))
	}
}
func main() {
	os := runtime.GOOS
	if os == "linux" {
		update()
		upgrade()
		autoremove()
	} else {
		fmt.Println("Nur für Windows!")
	}

}
